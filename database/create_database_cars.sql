create user rental@localhost identified by 'rental';
create database rental;
CREATE TABLE `rental`.`cars` ( `id` INT NOT NULL AUTO_INCREMENT , `model` VARCHAR(160) NOT NULL , `manufacturer` VARCHAR(160) NOT NULL , `year` INT NOT NULL , `uname` VARCHAR(20) NOT NULL, PRIMARY KEY (`id`));
CREATE TABLE `rental`.`users` (`uname` VARCHAR(10) NOT NULL , `password` VARCHAR(60) NOT NULL , `name` VARCHAR(60) NOT NULL , `su` BOOLEAN NOT NULL, PRIMARY KEY(`uname`));
ALTER TABLE `rental`.`cars` ADD CONSTRAINT `userdel` FOREIGN KEY (`uname`) REFERENCES `users`(`uname`) ON DELETE RESTRICT ON UPDATE RESTRICT;
INSERT INTO `rental`.`users` (`uname`, `password`, `name`, `su`) VALUES ('admin', 'admin', 'Superuser', TRUE);
INSERT INTO `rental`.`users` (`uname`, `password`, `name`, `su`) VALUES ('user1', 'user', 'User 1', FALSE);
INSERT INTO `rental`.`users` (`uname`, `password`, `name`, `su`) VALUES ('user2', 'user', 'User 2', FALSE);
INSERT INTO `rental`.`users` (`uname`, `password`, `name`, `su`) VALUES ('user3', 'user', 'User 3', FALSE);

-- https://en.wikipedia.org/wiki/List_of_production_cars_by_power_output
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Velo ', 'Benz', '1894', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Type 15 ', 'Peugeot', '1897', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Phoenix (de) ', 'Daimler', '1899', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('35 HP ', 'Mercedes', '1901', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Simplex ', 'Mercedes', '1902', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Simplex 60 HP ', 'Mercedes', '1903', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('60/75 ', 'Hispano-Suiza', '1907', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('80', 'Napier', '1908', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Fraschini Tipo KM ', 'Isotta', '1910', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('82/200 HP (de) ', 'Benz', '1912', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Model J ', 'Duesenberg', '1928', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Model SJ ', 'Duesenberg', '1932', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Model SSJ ', 'Duesenberg', '1935', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Monterey ', 'Mercury', '1958', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Fury Max Wedge Ramcharger II ', 'Plymouth', '1963', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Cobra Mk. III 427 Competition ', 'Shelby', '1965', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('EB110 ', 'Bugatti', '1991', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('F1 ', 'McLaren', '1992', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('F1 LM ', 'McLaren', '1995', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('CCR ', 'Koenigsegg', '2004', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Veyron ', 'Bugatti', '2005', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Ultimate Aero TT ', 'SSC', '2009', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('One:1 ', 'Koenigsegg', '2014', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Regera ', 'Koenigsegg', '2016', 'admin');
INSERT INTO `rental`.`cars` (model, manufacturer, year, uname) VALUES('Chiron Super Sport 300+/Centodieci ', 'Bugatti', '2022', 'admin');

grant all privileges on rental.* to rental@localhost;
