# Library example (REST project)

### The project

This project has made as an exam task for Service Oriented Programming subject leaded by Dr. Király Sándor.

The solution has 3 layers:
- MySQL database
- Node.js API (PHP API added afterwards)
- WPF client application

Accordingly you can find 3 folders in the project.

### The recommended steps for the installation:

1) Get the source from GitLab repository. 
<br />From command line:   
*`> git clone https://gitlab.com/tanyilearn/library_example.git`*

2) Install a MySQL database   
XAMPP recommended: `https://www.apachefriends.org/download.html`
The project's database folder contains 2 SQL scripts:
	+ *`create_database.sql`* - for initial database and user creation
	+ *`drop_database.sql`* - for dropping the database and the user

	The scripts are UTF-8 encoded. Recommended command for the database creation: <br />
*`> mysql -u root --default-character-set=utf8 <create_database.sql`*

3) The API implementation developed on Node.js. The projects folder already contains the required modules. 
	+ Please download and install the Node.js 18.12.1 version from the [Node.js official website](https://nodejs.org/en/download/). 
<br />Direct link to the installer for Windows platform: [https://nodejs.org/dist/v18.12.1/node-v18.12.1-x64.msi](https://nodejs.org/dist/v18.12.1/node-v18.12.1-x64.msi).
	+ The API server script can be found at *`'api\LibAPI\LibAPI\server.js'`* path.<br />
	To start the server API you can use the following command line from the procet's root directory:
	<br /><br />`C:\library_example>`*`node api\LibAPI\LibAPI\server.js`*
	<br />`API server listening on http://127.0.0.1:8000`
	<br />`Database connection established.`<br /><br />
	
	
4) Although, it's much more easy to setup and run the Node.js API, I made a PHP API, too. So you can install PHP API instead of Node.js if you prefer that. The client is looking for the API server on the localhost on TCP port 8000 by default. So, there are some recommendation for XAMPP Apache server virtual host configuration:

	+ Add *`Listen 8000`* line to the *`'apache\conf\httpd.conf'`* file
	+ Add the following VirtualHost configuration to the *`'C:\xampp\apache\conf\extra\httpd-vhosts.conf'`* file:
		<br />`<VirtualHost *:8000>`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`DocumentRoot "C:\xampp\htdocs\LibPHP_API"`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`ServerName 127.0.0.1:8000`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`<Directory "C:\xampp\htdocs\LibPHP_API">`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`Options -Indexes +FollowSymLinks +MultiViews`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`AllowOverride All`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`Require all granted`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`</Directory>`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" combined`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`SetEnvIf Request_URI "(\/assets\/)" dontlog`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`CustomLog C:\xampp\apache\logs\myweb-access.log combined env=!dontlog`
		<br />&nbsp;&nbsp;&nbsp;&nbsp;`ErrorLog C:\xampp\apache\logs\myweb-error.log`
		<br />`</VirtualHost>`

	+ If the XAMPP installation directory not *`'C:\xampp'`* you need to give different path in the configuration file above accordingly.
	



5) The git repository already contains runnable *`LibClient.exe`* file in the *`'client\LibClient\LibClient\bin\Debug\'`* folder. Also you can open the project in Visual Studio 2022 of course. The solution file (*`Libclient.sln`*) can be found in the *`client\LibClient\`* directory.

<br />

### Some rules of the example project
- Admins (users with `su` permission) can add remove books, also can return books from any normal user
- Admins can't rent books.
- Rented books can't be deleted. (has to return first).
- Users can rent books and return their rented books but can't add them to the library, or remove them from the library.

<br />

### The users
Unfortunately, user management has not implemented, yet. The predefined users/passwords are:
- admin/admin
- user1/user
- user2/user
- user3/user

Those credentials are always turn up when the client starts. 

<br />

### Acknowledgement
Finally, I would like to thank Dr. Király Sándor for the inspiration and the gathered knowledge. This project would not have been possible without him. 

