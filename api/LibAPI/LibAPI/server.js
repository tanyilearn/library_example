'use strict';
var express = require('express');
var app = express();

var httpAddress = '127.0.0.1';
var httpPort = 8000;

var mysqlAddress = '127.0.0.1';
var mysqlPort = 3306;

var mysql = require('mysql');
var bodyParser = require('body-parser')


// app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// function Auth(success) {
//     this.success = success;
// };
//
// Auth.prototype.setSuccess = function(success) {
//         this.success = success;
// };

// http
var httpServer = app.listen(httpPort, httpAddress, function () {
    console.log('API server listening on http://'
        + httpServer.address().address + ':'
        + httpServer.address().port);
});


// mysql
var mysqlServer = mysql.createConnection({
    host: mysqlAddress,
    port: mysqlPort,
    user: 'library',
    password: 'library',
    database: 'library'
});

mysqlServer.connect(function (err) {
    if (err) {
        console.log('Can\'t connect to the database on host: ' +
            mysqlAddress + " and port:" + mysqlPort);
        console.log("Make sure mysql server is running and database created.");
        console.log("You can create initial database by running this from the project\'s \'database\' directory:");
        console.log("mysql -u root --default-character-set=utf8 <create_database.sql");
        process.exit();
    }
    console.log('Database connection established.');
});



// app.get('/books', function (req, res) {
//     getAllBooks(res);
// });

app.get('/freebooks', function (req, res) {
    getFreeBooks(res);
});

app.get('/auth', function (req, res, next) {
    clientAuth(req, res, next, getName);
});

app.get('/browserauth', function (req, res, next) {
    browserAuth(req, res, next, getRented);
});

app.get('/rented', function (req, res, next) {
    clientAuth(req, res, next, getRented);
});

app.put('/rent', function (req, res, next) {
    clientAuth(req, res, next, rent);
});

app.put('/return', function (req, res, next) {
    clientAuth(req, res, next, returning);
});

app.delete('/delete', function (req, res, next) {
    clientAuth(req, res, next, deleting);
});

app.post('/newbook', function (req, res, next) {
    clientAuth(req, res, next, adding);
});

// function getAllBooks(res) {
//     mysqlServer.query('select * from books', function (error, results) {
//         if (error) throw error;
//         res.json(results);
//     });
// }

function getFreeBooks(res) {
    mysqlServer.query('select * from books where uname=? order by title', ['admin'], function (error, results) {
        if (error) throw error;
        res.json(results);
    });
}

function getRented(req, res, auth, su) {
    // console.log("GET parameters: " + req.query.username.toString());
    if (su)
        mysqlServer.query('select * from books where uname != ? order by title', [auth], function (error, results) {
            if (error) throw error;
            res.json(results);
        });
    else
        mysqlServer.query('select * from books where uname=? order by title', [auth], function (error, results) {
            if (error) throw error;
            res.json(results);
        });
}

function rent(req, res, auth, su) {
    // console.log("PUT parameters: " + req.query.username + ", " + req.query.id);
    if (su)
        res.end();
    else
        mysqlServer.query('update books set uname = ? where id=? and uname=?',
            [auth, req.query.id, "admin"], function (error, results) {
                if (error) throw error;
                res.json(results);
        });
}

function returning(req, res, auth, su) {
    // console.log("PUT parameters: " + req.query.username + ", " + req.query.id);
    if (su)
        mysqlServer.query('update books set uname=? where uname=? AND id=?',
            ["admin", req.query.username, req.query.id], function (error, results) {
                if (error) throw error;
                res.json(results);
            });
    else
        mysqlServer.query('update books set uname=? where uname=? AND id=?',
            ["admin", auth, req.query.id], function (error, results) {
                if (error) throw error;
                res.json(results);
        });
}

function deleting(req, res, auth, su) {
    if (su)
        mysqlServer.query('delete from books where id=? AND uname=?',
            [req.query.id, "admin"], function (error, results) {
                if (error) throw error;
                res.json(results);
            });
    else
        res.end();
}

function adding(req, res, auth, su) {
    if (su)
        mysqlServer.query('insert into books (title, author, publication, uname) values(?, ? ,?, ?)',
            [req.query.title, req.query.author, req.query.publication, "admin"], function (error, results) {
                if (error) throw error;
                res.json(results);
            });
    else
        res.end();
}

function getSu(uname, callback) {
    mysqlServer.query('SELECT su from users where uname=?', [uname], function (err, result) {
        if (err)
            callback(err, null);
        else
            callback(null, result[0].su);
    });
}

function getName(req, res) {
    mysqlServer.query('select name, su from users where uname=?',[req.query.username], function (error, results) {
        if (error) throw error;
        res.json(results);
    });
}

function clientAuth(req, res, next, request) {
    var authHeader = req.headers.authorization;
    if (!authHeader) {
        res.end();
        return;
    }
    var auth = Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
    var username = auth[0];
    var password = auth[1];
    // console.log("Username: " + username + " / " + "Password: " + password);
    // console.log(authHeader);
    mysqlServer.query("select * from users where uname=? and password=?", [username, password], function (authErr, rows) {
        if (!authErr && rows.length === 1) {
            getSu(username, function(err, isSu) {
                if (err)
                    console.log("Error: ", err);
                else
                    request(req, res, username, isSu);
            });
        } else {
            var err = new Error('You are not authenticated')

            res.setHeader('WWW-Authenticate', 'Basic');
            err.status = 401
            res.end();
        }
    });
}

// function browserAuth(req, res, next, request) {
//     var authHeader = req.headers.authorization;
//     if (!authHeader) {
//         var err = new Error('You are not authenticated');
//
//         res.setHeader('WWW-Authenticate', 'Basic');
//         err.status = 401;
//         next(err);
//     }
//     console.log(authHeader);
//
//     var auth = Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
//     var username = auth[0];
//     var password = auth[1];
//
//     console.log("Password is: " + password)
//     mysqlServer.query("select * from users where uname=? and password=?", [username, password], function(authErr, rows) {
//         if (!authErr && rows.length === 1) {
//             console.log("Successful authentication");
//             getSu(username, function(err, isSu) {
//                 if (err)
//                     console.log("Error: ", err);
//                 else
//                     request(req, res, username, isSu);
//             });
//         } else {
//             var err = new Error('You are not authenticated')
//
//             res.setHeader('WWW-Authenticate', 'Basic');
//             err.status = 401
//             next(err)
//         }
//     });
//
// }
