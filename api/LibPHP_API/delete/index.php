<?php

include_once("../commons.php");
$request_method=$_SERVER["REQUEST_METHOD"];

if ($request_method == 'DELETE' &&!empty($_GET['id'])) {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        header('WWW-Authenticate: Basic realm="Library"');
        header('HTTP/1.0 401 Unauthorized');
    } else {
        $exist = exist($_GET['id'], 'admin');
        if ($exist) {
            $su = getSu($_SERVER['PHP_AUTH_USER']);
            if ($su == 1) {
                $query = "DELETE from books where id=" . $_GET['id']. " and uname='admin'";
                auth($query);
            }
        } else {
            header('Content-Type: application/json');
            echo json_encode($not_real_update);
        }
    }
}

?>