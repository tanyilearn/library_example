<?php

class connection
{
    var $address = "127.0.0.1";
    var $username = "library";
    var $password = "library";
    var $database = "library";
    var $conn;

    function getConnstring() {
        $con = mysqli_connect($this->address, $this->username, $this->password, $this->database)
        or die;

        if (mysqli_connect_errno()) {
            exit();
        } else {
            $this->conn = $con;
        }
        return $this->conn;
    }
}