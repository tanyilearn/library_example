﻿using System;
using System.Collections.Generic;
using System.Windows;
using RestSharp;
using RestSharp.Authenticators;

namespace LibClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string URL = "http://127.0.0.1:8000/";
        private static string FREE_BOOKS_ROUTE = "freebooks/";
        private static string RENTED_BOOKS_ROUTE = "rented/";
        private static string RENT_ROUTE = "rent/";
        private static string RETURN_ROUTE = "return/";
        private static string DELETE_ROUTE = "delete/";
        private static string NEW_BOOK_ROUTE = "newbook/";
        private static string AUTH_ROUTE = "auth/";
        private static string LOGIN_MESSAGE = "Please Log in to rent or return a book.";
        private static string DB_ISSUE_MESSAGE = "Database error occurred.\n\nBook lists will be refreshed.";

        private AddBookWindow addBookWindow;

        private class Book
        {
            public int id { get; set; }
            public string title { get; set; }
            public string author { get; set; }
            public int publication { get; set; }
            public string uname { get; set; }
        }

        private class  Affected
        {
            public int rows { get; set; }
        }

        private class AvailBookItem
        {
            public int Id { get; set; }
            public int Year { get; set; }
            public string Title { get; set; }
            public string Author { get; set; }
        }

        private class RentedBookItem
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Username { get; set; }
        }

        private class Auth
        {
            public string name { get; set; }
            public int su { get; set; }
        }

        private class Response
        {
            public int fieldCount { get; set; }
            public int affectedRows { get; set; }
            public int insertId { get; set; }
            public int serverStatus { get; set; }
            public int warningCount { get; set; }
            public string message { get; set; }
            public bool protocol41 { get; set; }
            public int changedRows { get; set; }
        }


        private void ApiError()
        {
            MessageBox.Show("API communication error.\n\nPlease check the API server.", "Error", 
                MessageBoxButton.OK, MessageBoxImage.Error);
            // System.Windows.Application.Current.Shutdown();
        }


        public MainWindow()
        {
            InitializeComponent();
            MessageBox.Show("Until there is no user management implemented\n" +
                "you can manipulate the users and passwords\n" +
                "directly from the database.\n\n" +
                "Default Users/Passwords:\n" +
                "- admin/admin\n" +
                "- user1/user\n" +
                "- user2/user\n" +
                "- user3/user\n\n" +
                "Thanks!", "Welcome");
            SetCommonControls(false);
            GetAllAvailalbleBooks();
        }

        private void SetCommonControls(bool loggedin, bool su = false)
        {
            Username_field.IsEnabled = !loggedin;
            Password_field.IsEnabled = !loggedin;
            Rent_button.IsEnabled = loggedin & !su;
            Return_button.IsEnabled = loggedin;
            AddBook_button.IsEnabled = loggedin & su;
            DeleteBook_button.IsEnabled = loggedin & su;
            Login_button.IsDefault = !loggedin;

            if (loggedin)
                RentedRefresh_button.Opacity = 1;
            else
                RentedRefresh_button.Opacity = 0.25;
            RentedRefresh_button.IsEnabled = loggedin;
        }

        private void ClearRentedView()
        {
            while (RentedBooks.Items.Count > 0)
            {
                RentedBooks.Items.RemoveAt(0);
            }
        }

        private void ClearAvailView()
        {
            while (AvailBooks.Items.Count > 0)
            {
                AvailBooks.Items.RemoveAt(0);
            }
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            
            if (Username_field.IsEnabled)
            {
                string name;
                bool su;
                bool error;
                bool success = Login(Username_field.Text, Password_field.Password, out name, out su, out error);
                if (success)
                {
                    // MessageBox.Show("Succesfull login.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    SetCommonControls(true, su);
                    Message.Content = "Hello " + name + "!";
                    Login_button.Content = "Log off";
                    GetRented(Username_field.Text, Password_field.Password);
                    GetAllAvailalbleBooks();
                }
                else if (!error)
                {
                    MessageBox.Show("Invalid credentials.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            } 
            else
            {
                SetCommonControls(false);
                Username_field.Text = "";
                Password_field.Password = "";
                Login_button.Content = "Log in";
                Message.Content = LOGIN_MESSAGE;
                ClearRentedView();
            }
            
        }

        private bool Login(string username, string password, out string name, out bool su, out bool error)
        {
            var client = new RestClient(URL);
            client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest(AUTH_ROUTE, Method.Get);
            request.AddParameter("username", username);
            request.RequestFormat = RestSharp.DataFormat.Json;
            RestResponse<List<Auth>> response = client.Execute<List<Auth>>(request);
            name = null;
            su = false;
            error = false;

            if (response.Content == null)
            {
                error = true;
                ApiError();
                return false;
            }

            if (response.Data != null)
            {
                foreach (Auth a in response.Data)
                {
                    name = a.name;
                    if (a.su != 0)
                        su = true;
                    else
                        su = false;
                }
                return true;
            } 
            else
            {
                return false;
            }
        }

        private void GetAllAvailalbleBooks()
        {
            var client = new RestClient(URL);
            var request = new RestRequest(FREE_BOOKS_ROUTE, Method.Get);
            request.RequestFormat = RestSharp.DataFormat.Json;
            RestResponse<List<Book>> response = client.Execute<List<Book>>(request);

            if (response.Data == null) ApiError();
            else
            {
                ClearAvailView();
                foreach (Book book in response.Data)
                    AvailBooks.Items.Add(new AvailBookItem { 
                        Id = book.id, 
                        Year = book.publication,
                        Title = book.title,
                        Author = book.author
                    });
            }
        }

        private void GetRented(string username, string password)
        {
            var client = new RestClient(URL);
            client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest(RENTED_BOOKS_ROUTE, Method.Get);
            request.AddParameter("username", username);
            request.RequestFormat = RestSharp.DataFormat.Json;
            RestResponse<List<Book>> response = client.Execute<List<Book>>(request);

            if (response.Data == null) ApiError();
            else
            {
                ClearRentedView();
                foreach (Book book in response.Data)
                    RentedBooks.Items.Add(new RentedBookItem { Id = book.id, Title = book.title, Username = book.uname });
            }
            RentedBooks.Items.Refresh();
        }

        private void AvailRefresh_button_Click(object sender, RoutedEventArgs e)
        {
            GetAllAvailalbleBooks();
        }

        private void RentedRefresh_button_Click(object sender, RoutedEventArgs e)
        {
            if (Username_field.Text.Length > 0)
                GetRented(Username_field.Text, Password_field.Password);
        }

        private void Rent_button_Click(object sender, RoutedEventArgs e)
        {
            if (AvailBooks.SelectedItems.Count > 0)
            {
                foreach (AvailBookItem book in AvailBooks.SelectedItems)
                {
                    var client = new RestClient(URL);
                    client.Authenticator = new HttpBasicAuthenticator(Username_field.Text, Password_field.Password);

                    var request = new RestRequest(RENT_ROUTE, Method.Put);

                    request.AddParameter("username", Username_field.Text, ParameterType.QueryString);
                    request.AddParameter("id", book.Id.ToString(), ParameterType.QueryString);
                    request.RequestFormat = RestSharp.DataFormat.Json;
                    var response = client.Execute(request);

                    int rows = ResponseToAffectedRows(response);
                    if (rows == 0)
                    {
                        MessageBox.Show(DB_ISSUE_MESSAGE, "Error",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                        GetAllAvailalbleBooks();
                        GetRented(Username_field.Text, Password_field.Password);
                        return;
                    }
                }
                GetAllAvailalbleBooks();
                GetRented(Username_field.Text, Password_field.Password);
                MessageBox.Show("Book(s) rented successfully.", "Success",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Return_button_Click(object sender, RoutedEventArgs e)
        {
            if (RentedBooks.SelectedItems.Count > 0)
            {
                foreach (RentedBookItem book in RentedBooks.SelectedItems)
                {
                    var client = new RestClient(URL);
                    client.Authenticator = new HttpBasicAuthenticator(Username_field.Text, Password_field.Password);

                    var request = new RestRequest(RETURN_ROUTE, Method.Put);

                    request.AddParameter("username",book.Username, ParameterType.QueryString);
                    request.AddParameter("id", book.Id.ToString(), ParameterType.QueryString);
                    request.RequestFormat = RestSharp.DataFormat.Json;
                    var response = client.Execute(request);

                    int rows = ResponseToAffectedRows(response);
                    if (rows == 0)
                    {
                        MessageBox.Show(DB_ISSUE_MESSAGE, "Error",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                        GetAllAvailalbleBooks();
                        GetRented(Username_field.Text, Password_field.Password);
                        return;
                    }
                }
                GetAllAvailalbleBooks();
                GetRented(Username_field.Text, Password_field.Password);
                MessageBox.Show("Book(s) returned successfully.", "Success",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void AddBook_button_Click(object sender, RoutedEventArgs e)
        {
            addBookWindow = new AddBookWindow();
            AddBook_button.IsEnabled = false;
            Login_button.IsEnabled = false;
            addBookWindow.Closed += AddBookWindowClosed();
            addBookWindow.Show();
        }

        private EventHandler AddBookWindowClosed()
        {
            return new EventHandler((sender, e) => { 
                AddBook_button.IsEnabled = true;
                Login_button.IsEnabled = true;
            });
        }

        private void DeleteBook_button_Click(object sender, RoutedEventArgs e)
        {
            if (AvailBooks.SelectedItems.Count > 0)
            {
                foreach (AvailBookItem book in AvailBooks.SelectedItems)
                {
                    var client = new RestClient(URL);
                    client.Authenticator = new HttpBasicAuthenticator(Username_field.Text, Password_field.Password);

                    var request = new RestRequest(DELETE_ROUTE, Method.Delete);

                    request.AddParameter("id", book.Id.ToString(), ParameterType.QueryString);
                    request.RequestFormat = RestSharp.DataFormat.Json;
                    var response = client.Execute(request);

                    int rows = ResponseToAffectedRows(response);
                    if (rows == 0)
                    {
                        MessageBox.Show(DB_ISSUE_MESSAGE, "Error",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                        GetAllAvailalbleBooks();
                        GetRented(Username_field.Text, Password_field.Password);
                        return;
                    }
                        
                }
                GetAllAvailalbleBooks();
                GetRented(Username_field.Text, Password_field.Password);
                MessageBox.Show("Book(s) deleted successfully.", "Success",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private int ResponseToAffectedRows(RestResponse response)
        {
            return int.Parse(response.Content.ToString().Split(',')[1].Split(':')[1]);
        }

        public bool AddBook(string title, string author, int publication)
        {
            var client = new RestClient(URL);
            client.Authenticator = new HttpBasicAuthenticator(Username_field.Text, Password_field.Password);

            var request = new RestRequest(NEW_BOOK_ROUTE, Method.Post);

            request.AddParameter("title", title, ParameterType.QueryString);
            request.AddParameter("author", author, ParameterType.QueryString);
            request.AddParameter("publication", publication, ParameterType.QueryString);
            request.RequestFormat = RestSharp.DataFormat.Json;
            RestResponse response = client.Execute(request);

            int rows = ResponseToAffectedRows(response);
            if (rows == 0)
                return false;
            GetAllAvailalbleBooks();
            MessageBox.Show("Book added successfully.", "Success", 
                MessageBoxButton.OK, MessageBoxImage.Information);
            return true;
        }
    }
}
