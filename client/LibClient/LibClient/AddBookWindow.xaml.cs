﻿using RestSharp.Authenticators;
using RestSharp;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;


namespace LibClient
{
    /// <summary>
    /// Interaction logic for AddBookWindow.xaml
    /// </summary>
    public partial class AddBookWindow : Window
    {
       
        public AddBookWindow()
        {
            InitializeComponent();
        }

        private void YearValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void NewCancel_button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void NewOK_button_Click(object sender, RoutedEventArgs e)
        {
            string message;
            if (!IsValid(out message))
                MessageBox.Show(message, "Invalid input", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                if (!((MainWindow)Application.Current.MainWindow).AddBook(Title_field.Text, Author_field.Text, int.Parse(Year_field.Text)))
                    MessageBox.Show("Can't add the new book.", "SQL error", MessageBoxButton.OK, MessageBoxImage.Error);
                else
                    Close();
            }
        }

        private bool IsValid(out string message)
        {
            message = "";
            if (Title_field.Text.Length == 0) message += "Title field can't be empty.\n";
            if (Author_field.Text.Length == 0) message += "Author field can't be empty.\n";
            if (Year_field.Text.Length == 0) message += "Published field can't be empty.\n";
            if (message.Length > 0)
                return false;
            return true;
        }
    }
}
