﻿using RestSharp.Authenticators;
using RestSharp;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;


namespace CarClient
{
    /// <summary>
    /// Interaction logic for AddCarWindow.xaml
    /// </summary>
    public partial class AddCarWindow : Window
    {
       
        public AddCarWindow()
        {
            InitializeComponent();
        }

        private void YearValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void NewCancel_button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void NewOK_button_Click(object sender, RoutedEventArgs e)
        {
            string message;
            if (!IsValid(out message))
                MessageBox.Show(message, "Invalid input", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                if (!((MainWindow)Application.Current.MainWindow).AddCar(Model_field.Text, Manufacturer_field.Text, int.Parse(Year_field.Text)))
                    MessageBox.Show("Can't add the new car.", "SQL error", MessageBoxButton.OK, MessageBoxImage.Error);
                else
                    Close();
            }
        }

        private bool IsValid(out string message)
        {
            message = "";
            if (Model_field.Text.Length == 0) message += "Model field can't be empty.\n";
            if (Manufacturer_field.Text.Length == 0) message += "Manufacturer field can't be empty.\n";
            if (Year_field.Text.Length == 0) message += "Published field can't be empty.\n";
            if (message.Length > 0)
                return false;
            return true;
        }
    }
}
